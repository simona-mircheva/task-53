import "../scss/app.scss";

window.addEventListener("DOMContentLoaded", () => {
  // This block will be executed once the page is loaded and ready
  let productElements = document.querySelectorAll('.product')
  for (let i = 0; i < productElements.length; i++) {
    let price = productElements[i].querySelector('.price').textContent
    productElements[i].setAttribute('data-price', price)
    console.log(price);
  }
})

